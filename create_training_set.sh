#!/bin/sh

i=0
for l in A B C D E F G H I J K L M N O P Q R S T U V W X Y Z a b c d e f g h i j k l m n o p q r s t u v w x y z 0 1 2 3 4 5 6 7 8 9 + /; do
#for l in A; do
	sed 's|%LETTER%|'$l'|' letter_template.svg > letter.svg
	dir=training_set/$(printf %02d $i)
	mkdir -p $dir
	j=0
	for deg in 0; do
		for x in -3 -2 -1 +0 +1 +2 +3; do
			for y in -3 -2 -1 +0 +1 +2 +3; do
				#convert -background white -alpha remove -rotate $deg -blur 0x4 -gravity Center -crop 53x99+0+0 letter.svg $dir/$j.png
				convert letter.svg -page $x$y -background white -alpha remove -flatten -blur 0x4 $dir/$(printf %03d $j).png
				j=$((j+1))
			done
		done
	done
	i=$((i+1))
done
